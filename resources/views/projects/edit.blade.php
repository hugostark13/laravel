@extends('layout')

@section('content')
<form class="section" method="POST" action="/projects/{{ $project->id }}">
    @csrf      
    @method('PUT')
   <div class="field">
    <h1 class="label">Edit Project</h1>
    <label class="label">Title</label>
    <div class="control">
      <input class="input" type="text" id="title" name="title" value="{{ $project->title }}" placeholder="Title">
    </div>
  </div>
  
  <div class="field">
    <label class="label">Description</label>
    <div class="control">
      <input class="input" type="text" id="description" name="description" value="{{ $project->description }}" placeholder="Description">
    </div>
  </div>

  <div class="field">
    <label class="label">Client</label>
    <div class="control">
      <input class="input" type="text" id="client" name="client" value="{{ $project->client }}" placeholder="Client">
    </div>
  </div>

  <div class="field">
    <label class="label">Company</label>
    <div class="control">
      <input class="input" type="text" id="company" name="company" value="{{ $project->company }}" placeholder="Company">
    </div>
  </div>



  <div class="field">
    <label class="label">Status</label>
    <div class="control">
      <select name="status" id="status">        

        <option value="new">New</option>
        <option value="Pending">Pending</option>
        <option value="Failed">Failed</option>
        <option value="Done">Done</option>           
      

      </select>
    </div>
  </div>  

  <div class="field">
    <label class="label">Duration</label>
    <div class="control">
      <input class="input" type="number" id="duration" name="duration" value="{{ $project->duration }}" placeholder="Duration in weeks">
    </div>
  </div>  
  
  <div class="field is-grouped">
    <div class="control">
      <button class="button is-link">Submit</button>
    </div>  
  </div>
</form>
@endsection