@extends('layout')

@section('content')
<form class="section" method="POST" action="/projects">
    @csrf      
   <div class="field">
    <h1 class="label">New Project</h1>
    <label class="label">Title</label>
    <div class="control">
      <input class="input" type="text" id="title" name="title" placeholder="Title">
    </div>
  </div>
  
  <div class="field">
    <label class="label">Description</label>
    <div class="control">
      <input class="input" type="text" id="description" name="description" placeholder="Description">
    </div>
  </div>

  <div class="field">
    <label class="label">Client</label>
    <div class="control">
      <input class="input {{ $errors->has('client') ? 'is-danger' : '' }}" type="text" id="client" name="client" placeholder="Client">
      
      @if($errors->has('client'))
        <p class="help is-danger">{{ $errors->first('client') }}</p>
      @endif

    </div>
  </div>

  <div class="field">
    <label class="label">Company</label>
    <div class="control">
      <input class="input {{ $errors->has('company') ? 'is-danger' : '' }}" type="text" id="company" name="company" placeholder="Company">

      @if($errors->has('company'))
        <p class="help is-danger">{{ $errors->first('company') }}</p>
      @endif

    </div>
  </div>



  <div class="field">
    <label class="label">Status</label>
    <div class="control">
      <select name="status" id="status">        

        <option value="new">New</option>
        <option value="Pending">Pending</option>
        <option value="Failed">Failed</option>
        <option value="Done">Done</option>           
      

      </select>
    </div>
  </div>  

  <div class="field">
    <label class="label">Duration</label>
    <div class="control">
      <input class="input" type="number" id="duration" name="duration" placeholder="Duration in weeks">
    </div>
  </div>  
  
  <div class="field is-grouped">
    <div class="control">
      <button class="button is-link">Submit</button>
    </div>  
  </div>
</form>
@endsection