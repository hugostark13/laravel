@extends('layout')

@section('content')
<form class="section" method="POST" action="/articles/{{ $article->id }}">
    @csrf  
    @method('PUT')

    <h1>Edit Article</h1>

   <div class="field">
    <label class="label">Title</label>
    <div class="control">
      <input class="input" type="text" id="title" name="title" value="{{ $article->title }}">
    </div>
  </div>
  
  <div class="field">
    <label class="label">Excerpt</label>
    <div class="control">
      <input class="input" type="text" id="exerpt" name="exerpt" value="{{ $article->exerpt }}">
    </div>
  </div>
  
  <div class="field">
    <label class="label">Body</label>
    <div class="control">
      <input class="textarea" type="text" id="body" name="body" value="{{ $article->body }}">
    </div>
  </div>  
  
  <div class="field is-grouped">
    <div class="control">
      <button class="button is-link">Submit</button>
    </div>  
  </div>
</form>
@endsection