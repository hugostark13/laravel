@extends('layout')

@section('content')

 

   <div class="field">
    <label class="label">Title</label>
    <div class="control">
      <input class="input" type="text" id="title" name="title" value="{{ $article->title }}">
    </div>
  </div>
  
  <div class="field">
    <label class="label">Excerpt</label>
    <div class="control">
      <input class="input" type="text" id="exerpt" name="exerpt" value="{{ $article->exerpt }}">
    </div>
  </div>
  
  <div class="field">
    <label class="label">Body</label>
    <div class="control">
      <input class="textarea" type="text" id="body" name="body" value="{{ $article->body }}">
    </div>
  </div>  


@endsection