@extends('layout')

@section('content')
<form class="section" method="POST" action="/articles">
    @csrf      
   <div class="field">
    <h1>New article</h1>
    <label class="label">Title</label>
    <div class="control">
      <input class="input" type="text" id="title" name="title" placeholder="Text input">
    </div>
  </div>
  
  <div class="field">
    <label class="label">Excerpt</label>
    <div class="control">
      <input class="input" type="text" id="exerpt" name="exerpt" placeholder="Text input">
    </div>
  </div>
   

  <div class="field">
    <label class="label">Body</label>
    <div class="control">
      <input class="textarea" type="text" id="body" name="body" placeholder="Text input">
    </div>
  </div>  

  <div class="field">
    <label class="label">Tags</label>
    <div class="control">
      <select name="tags[]" multiple>

        @foreach ($tags as $tag)

        <option value="{{ $tag->id }}">{{ $tag->name }}</option>
            
        @endforeach

      </select>
    </div>
  </div>  
  
  <div class="field is-grouped">
    <div class="control">
      <button class="button is-link">Submit</button>
    </div>  
  </div>
</form>
@endsection