<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id'); 
            $table->unsignedBigInteger('project_id');           
            $table->string('title');
            $table->text('description');
            $table->string('status');
            $table->integer('duration')->nullable();
            $table->timestamps();           
           
        });

        Schema::create('projects_tasks', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->unsignedBigInteger('project_id');
            $table->unsignedBigInteger('tasks_id');          
            $table->timestamps(); 
            
            $table->unique(['project_id', 'tasks_id']);

            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('tasks_id')->references('id')->on('tasks')->onDelete('cascade');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
