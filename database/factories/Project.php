<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Project::class, function (Faker $faker) {
    return [       
        'title' => $faker->sentence,
        'description' =>  $faker->sentence,
        'client' => $faker->name,
        'company' => $faker->company,
        'status' => $faker->randomElement($array = array ('new','pending','failed', 'done'), $count = 1),
        'duration' => $faker->randomDigit,
    ];
});
