<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Project;

$factory->define(App\Task::class, function (Faker $faker) {
    return [  
        'project_id' => factory(App\Project::class),     
        'title' => $faker->sentence,
        'description' =>  $faker->sentence,        
        'status' => $faker->randomElement($array = array ('new','pending','failed', 'done'), $count = 1),
        'duration' => $faker->randomDigit,
    ];
});
