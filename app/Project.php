<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function task()
    {
    
       return $this->belongsToMany(Task::class);
        
    }

}
