<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

        return view('welcome');

  })->middleware('auth');

Route::post('/projects', 'ProjectsController@store');
Route::get('/projects/create', 'ProjectsController@create');
Route::get('/projects/{project}', 'ProjectsController@show');
Route::get('/projects/{project}/edit', 'ProjectsController@edit');
Route::put('/projects/{id}', 'ProjectsController@update');
Route::get('/api/projects', 'ProjectsController@index');
Route::get('/api/tasks', 'TasksController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
